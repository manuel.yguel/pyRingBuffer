"""
Copyright (c) <2016> <Manuel Yguel, Strataggem R&D lab>

MIT Licence

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""



"""Provides a very simple ring buffer or circular buffer with fixed size.
The ring buffer is initialized by None objects.
"""
class RingBuffer:
    def __init__(self,N):
        self.reset(N)
    
    def __len__(self):
        """Returns the size of the circular buffer
        """
        return self.N

    def reset(self,M=None):
        """Clear the ring buffer and set its size. Default new size is the
        old size. The ring buffer is initialized by None objects.
        """
        if( None != M ):
            self.N=M
        self.window = [None] * self.N
        self.index = 0

    def push(self,x):
        """Inserts a new element at the end of the circular buffer. If the
        place is occupied, returns the element at this place, the one that is replaced by the new element.
        """
        a = self.window[self.index]
        self.window[self.index]=x
        self.index = (self.index+1)%self.N
        return a

    def last(self):
        """Returns the last element inserted.
        """
        return self.window[(self.index-1)%self.N]

    def first(self):
        """Returns the next element to be replaced.
        """
        return self.window[self.index]
    
    def __iter__(self):
        """Returns an Iterator object """
        return RingBufferIterator(self)
    
    def __getitem__(self,index):
        """Returns the element at index where index 0 is the oldest element 
        or the next element to be replaced.
        In other words it allows to access the buffer from oldest element to
        newest element in chronological order of insertion. """
        idx = (self.index + index) % self.N
        return self.window[idx]
    
    def __contains__(self,e):
        """Returns true if e is in the ring buffer"""
        return e in self.window
    
    def middle(self): 
        """Returns the center of the window if the window has an odd size 
           or a pair of values arround the center
        """
        if 1 == self.N % 2: 
            diag = int((self.N -1)/2)
            middle = (self.index + diag)%self.N
            return [self.window[middle]] 
        else: 
            diag = int((self.N)/2)
            m1 = (self.index + diag-1)%self.N
            m2 = (self.index + diag)%self.N
            return [self.window[m1],self.window[m2]]


class RingBufferIterator:
    ''' Iterator class for RingBuffer '''
    def __init__(self,ring):
        self._ring = ring
        self._index = 0

    def __next__(self):
        ''' Returns the next value from RingBuffer object's list '''
        if self._index < len(self._ring):
            result = self._ring[self._index]
            self._index += 1
            return result
        # End of Iteration
        raise StopIteration
    
    def __iter__(self):
        return self
