# Ring Buffer

tags: ring buffer, circular buffer, rolling window, running window

Provides a very simple ring buffer or circular buffer with fixed size.
The ring buffer is initialized by None objects.

## Usage example

```python
from ringbuf import *

#The only parameter is the size of the window
rb = RingBuffer(5)
rb.push(1)
rb.push(2)
rb.push(3)
rb.push(4)
rb.push(5)

#When a new element takes the place of an old one, the push method returns the old element
#here it returns 1
rb.push(6)

#last method returns the last inserted element
#here it returns 6
rb.last()

#first method returns the next element to be replaced
#here it returns 2
rb.first()

#here it returns 2
rb.push(7)

#middle method returns a list with the center of the window for an odd sized window and
# the two elements around the 'virtual' center in case of an even sized window
#here it returns [5] because the windows ordered by inserting time is [3,4,5,6,7]
# in memory it is still [6,7,3,4,5] but the class keeps track of the moving middle.
rb.middle()

#here it return the middle element
rb.middle()[0]

#To see the content of the buffer
print( rb.window )

#To re-initialize the ring buffer with None objects
rb.reset()
print( rb.window )

```


Use standard python idioms and operators:

```python
from ringbuf import *

#The only parameter is the size of the window
rb = RingBuffer(3)
rb.push(1)
rb.push(2)
rb.push(3)
rb.push(4)

#Returns 2
rb[0]

#Returns 3
rb[4]

#Iterates over the window starting for oldest element inserted up to newest
#inserted element.
for e in rb:
    print(e)

#Returns True
4 in rb

#Returns False
1 in rb

```