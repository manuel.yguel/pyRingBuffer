#! /usr/bin/python3
import unittest

if None == __package__:
    from context import ringbuf as ring
else:
    from .context import ringbuf as ring
 
class TestRingBufferMethods(unittest.TestCase):
 
    def test_5_elements(self):
        rb = ring.RingBuffer(5)
        self.assertEqual(5, len(rb))
        rb.push(1)
        rb.push(2)
        rb.push(3)
        rb.push(4)
        rb.push(5)
        self.assertFalse(6 in rb)
        for i in range(0,len(rb)):
            self.assertEqual(i+1,rb[i])
        i=1
        for e in rb:
            self.assertEqual(i,e)
            i+=1
        self.assertEqual(1,rb.push(6))
        self.assertEqual(6,rb.last())
        self.assertEqual(2,rb.first())
        self.assertEqual(2,rb.push(7))
        self.assertEqual([5],rb.middle())
        rb.reset()
        for i in range(0,len(rb)):
            self.assertEqual(None,rb[i])
    
    def test_3_elements(self):
        rb = ring.RingBuffer(3)
        rb.push(1)
        rb.push(2)
        rb.push(3)
        rb.push(4)

        self.assertEqual(2,rb[0])
        self.assertEqual(3,rb[4])
        self.assertTrue(4 in rb)
        self.assertFalse(1 in rb)
 
if __name__ == '__main__':
    unittest.main()